var LikeChecker = {
	getUser: function(id, onStarted, onDone, onFailed) {
		onStarted(id);
		VK.Api.call("execute.getUser", { id: id, func_v: 2, v: 5.53}, function(result) {
			if(result.response) {
				var user = result.response;
				var counters = user.data.counters;
				var rePages = new Array(counters.friends + counters.groups
					+ counters.subscriptions + counters.pages);
				var index = 0;
				
				for (i = 0; i < user.pages.groups.length; i++) {
					rePages[index++] = "-" + user.pages.groups[i];
				}
				for (i = 0; i < user.pages.users.length; i++) {
					rePages[index++] = "" + user.pages.users[i];
				}
				
				user.pages = rePages;
				onDone(user);
			} else if (result.execute_errors) {
				onFailed(result.execute_errors);
			}
		});
	}, 
	
	getFriends: function(onStarted, onDone, onFailed) {
		onStarted();
		VK.Api.call("friends.get", { order: "hints", fields: "photo_50,screen_name", v: 5.53}, function(result) {
			if(result.response) {
				onDone(result.response.items);
			} else if (result.execute_errors) {
				onFailed(result.errors);
			}
		});
	}
}

/*	***************
		SCANNER
	**************	*/
LikeChecker.Scanner = function(userId, interval, needPhotos, pages, 
	onStarted, onProgress, onDone, onCanceled) {
	this.params = {
		userId: userId,
		interval: interval,
		needPhotos: needPhotos
	}	
	this.pages = pages;
	this.state = {
		currentPage: 0,
		totalPages: this.pages.length,
		found: 0,
		isRunning: false,
		isCanceled: false
	};
	this.onStarted = onStarted;
	this.onProgress = onProgress;
	this.onDone = onDone; //(found)
	this.onCanceled = onCanceled;
};

LikeChecker.Scanner.prototype.start = function() {
	
	if (this.state.isCanceled) {
		throw "This scanner was canceled and now outdated";
	}
	this.onStarted();
	this.state.isRunning = true;
	this.onProgress(this.state, []);
	
	var scanPage = function(pageId, onPageDone) {
		var request = function(method, offset, onRequestDone) {
			VK.Api.call(method, { pageId: pageId, offset: offset, 
				userId: this.params.userId, interval: this.params.interval, func_v: 2, v: 5.53}, 
				function(result) {
					var newPosts = [];
					var outOfInterval = false;
					if(result.response) {
						var response = result.response;
						var newCount = 0;
						
						for (i = 0; i < response.length; i++) {
							if (response[i].date > 0) {
								newPosts[newCount++] = response[i];
							} else {
								outOfInterval = true;
								break;
							}
						}
						this.state.found += newCount;
					}
					onRequestDone(method, newPosts, outOfInterval, result.error);
			}.bind(this));
			
		}.bind(this);
		
		var offset = 0;
		let photosScanned = false;
		var pagePosts = [];
		var onRequestDone = function(method, newPosts, outOfInterval, error) {
			for (i = 0; i < newPosts.length; i++) {
				pagePosts[pagePosts.length] = newPosts[i];
			}
			if (!this.state.isCanceled) {
				if (error) {
					if (error.error_code == 6) {
						setTimeout(function() { request(method, offset, onRequestDone); }, 400);
						return;
					} else {
						console.log("Error " + error.error_msg);
					}
				} else {
					if (!outOfInterval) {
						offset += 23;
						request(method, offset, onRequestDone);
						return;
					}
				}

				if (parseInt(pageId) > 0 && this.params.needPhotos && !photosScanned) {
					photosScanned = true;
					offset = 0;
					request("execute.checkphotos", offset, onRequestDone);
					return;
				}
			}
			
			onPageDone(pagePosts);
		}.bind(this);
		
		request("execute.checkwallE", offset, onRequestDone);
		
	}.bind(this);

	var onPageDone = function(pagePosts) {
		this.onProgress(this.state, pagePosts);
		if (!this.state.isCanceled) {
			this.state.currentPage++;
			if (this.state.currentPage < this.state.totalPages) {
				scanPage(this.pages[this.state.currentPage], onPageDone);
			} else {
				this.state.isRunning = false;
				this.onDone(this.state.found);
			}
		} else {
			this.state.isRunning = false;
			this.onCanceled();
		}
	}.bind(this);
	
	scanPage(this.pages[this.state.currentPage], onPageDone);
};
	
LikeChecker.Scanner.prototype.cancel = function() {
	this.state.isCanceled = true;
};
