var Session = { loadedPh7: false }
var View = {
	init: function() {
		this.fixedScanStateVisible = false;
		
		this.scanParams = $("#scanParams");
		this.userName = $("#userName");
		this.userPhoto = $("#userPhoto");
		this.userSubsCount = $("#userSubsCount");
		this.userFriendsCount = $("#userFriendsCount");
		this.userGroupsCount = $("#userGroupsCount");
		this.loginModal = $("#login");
		this.pagesModal = $("#pages");
		this.loadingSplash = $("#loadingSplash");
		this.idSplash = $("#idSplash");
		this.userId = $("#userId");
		this.scanParamInterval = $("#scanParamInterval");
		this.scanParamPhotos = $("#scanParamPhotos");
		this.getUserProgress = $("#getUserProgress");
		this.foundPosts = document.getElementById("foundPosts");//$("#foundPosts");
		this.fixedScanState = $("#fixedScanState");
		this.scanStateCurrent = $(".scanStateCurrent");
		this.scanStateFound = $(".scanStateFound");
		this.toggleScanButton = $("#toggleScanButton");
		this.scanTopState = $("#scanTopState");
		this.friendsSplash = $("#friendsSplash");
		this.getFriendsProgress = $("#getFriendsProgress");
		this.pagesList = $("#pagesList");
		
		$(document).on("scroll", function(event) {
			let needShowState = (Session.scanner && $(document).scrollTop() > this.scanParams.height());
			if (needShowState && !this.fixedScanStateVisible) {
				this.fixedScanState.addClass("scanState-visible");
			this.fixedScanStateVisible = true;
			} else if (!needShowState && this.fixedScanStateVisible) {
				this.fixedScanState.removeClass("scanState-visible");
				this.fixedScanStateVisible = false;
			}
		}.bind(this));

		$(document).ready(function() {
			$(".modal-trigger").leanModal();
		});
	},
	
	setUser: function(userData) {
		let userName = userData.first_name + " " + userData.last_name;
		this.userName.text(userName);
		this.userPhoto.attr("alt", userName);
		
		let displayUserPhoto = function(photoUrl) {
			var photo = new Image();
			photo.onload = function(){
				Materialize.fadeInImage('#userPhoto');
				this.userPhoto.attr("src", photo.src); 
			}.bind(this);
			photo.src = photoUrl;
		}.bind(this);
		displayUserPhoto(userData.photo_200);
		
		let counters = userData.counters;
		this.userSubsCount.text(counters.subscriptions + counters.pages);
		this.userFriendsCount.text(counters.friends);
		this.userGroupsCount.text(counters.groups);
	},
	
	showLoginModal: function() {
		this.loginModal.openModal({
		dismissible: false,
		opacity: .7});
	},

	showPagesModal: function() {
		this.pagesModal.openModal({
		dismissible: false,
		opacity: .7});
		this.pagesList.focus();
	},
	
	showFriendsSplash: function() {
		this.friendsSplash.css("display", "block");
	},
	
	hideFriendsSplash: function() {
		this.friendsSplash.css("display", "none");
	},
	
	hideLoadingSplash: function() {
		this.loadingSplash.css("display", "none");
	},
	
	showIdSplash: function() {
		this.idSplash.css("display", "block");
		hideGetUserProgress();
	},
	
	hideIdSplash: function() {
		this.idSplash.css("display", "none");
	},
	
	showGetUserProgress: function() {
		this.getUserProgress.css("display", "block");
	},
	
	hideGetUserProgress: function() {
		this.getUserProgress.css("display", "none");
	}, 
	
	getScanParams: function() {
		if (!this.scanParamInterval[0].checkValidity()) {
			return null;
		}
		return {
			days: this.scanParamInterval.val(),
			needPhotos: this.scanParamPhotos.prop("checked")
		};
	},
	
	addPosts: function(posts) {
		let process = function async(posts, callback) {
			let fragment = document.createDocumentFragment();
			
			for (i = 0; i < posts.length; i++) {
				let post = posts[i];
				let postCard = document.createElement("post-card");
				postCard.setAttribute("class", "post");
				postCard.setAttribute("author", JSON.stringify(post.author));
				postCard.setAttribute("date", post.date);
				postCard.setAttribute("type", post.post_type);
				postCard.setAttribute("post-text", post.text);
				postCard.setAttribute("url", post.url);
				if (post.copy_history) {
					postCard.setAttribute("is-repost", true);
					postCard.setAttribute("repost-text", post.copy_history[0].text);
					postCard.setAttribute("repost-author-name", post.repostAuthor.name);
					
					postCard.setAttribute("atts", JSON.stringify( post.copy_history[0].attachments));
				} else {
					postCard.setAttribute("atts", JSON.stringify(post.attachments));
				}
				fragment.appendChild(postCard);
			}
			callback(fragment);
		}
		
		process(posts, function(fragment) {
			this.foundPosts.appendChild(fragment);
		}.bind(this));
	},

	addPh7: function(data) {
		let postCard = document.createElement("post-card");
		postCard.setAttribute("class", "post");
		postCard.setAttribute("type", "sponsored");
		postCard.setAttribute("sponsored-content", data);
		this.foundPosts.appendChild(postCard);
	},	
	
	clearFoundPosts: function() {
		this.foundPosts.innerHTML = "";
	},
	
	setScanStarted: function() {
		this.toggleScanButton.text("Остановить");
		this.scanTopState.css("display", "inline");
	},
	
	setScanState: function(state) {
		this.scanStateCurrent.text("Страница " + (state.currentPage + 1) + "/" + state.totalPages);
		this.scanStateFound.text("Найдено " + state.found);
	},
	
	setScanDone: function() {
		let doneText = "Завершено!";
		this.scanStateCurrent.text(doneText);
		this.toggleScanButton.text("Начать");
	},
	
	setScanCanceled: function() {
		let canceledText = "Остановлено";
		Materialize.toast(canceledText, 2000);
		this.scanStateCurrent.text(canceledText);
		this.toggleScanButton.text("Начать");
	}
}

function initApp() {
	View.init();
	VK.init({apiId: 3778000});
	FlurryAgent.startSession("NKPRXTYB86QQT2PFZ792");
	FlurryAgent.logEvent("Environment", {"platform": navigator.platform});
	VK.Auth.getLoginStatus(function (response) {
		if (response.status != "connected") {
			View.showLoginModal();
		} else {
			FlurryAgent.setUserId(response.session.mid);
			View.hideLoadingSplash();
			Notification.requestPermission();
		}
	});
}

function login() {
	VK.Auth.login(function(response) {
		if (response.session) {
			location.reload(true);
		}
	}, 270340);
}

function getFriends() {
	LikeChecker.getFriends(onGetFriendsStarted, onGetFriendsDone, onGetFriendsFailed);
}

function onGetFriendsStarted() {
	View.showFriendsSplash();
}

function onGetFriendsDone(friends) {
	let options = {
		valueNames: ["first_name", "last_name", "screen_name", { attr: "src", name: "photo_50" }],
		item: '<li class="collection-item avatar"><a href="" onclick="onFriendSelected(this); return false;"><img class="photo_50 circle" alt=""><span><span class="first_name"></span>&nbsp;<span class="last_name"></span></span><p class="screen_name"></p></a></li>'
	};
	let friendsList = new List("friendsList", options);
	friendsList.clear();
	friendsList.add(friends);
}

function onFriendSelected(item) {
	View.hideFriendsSplash();
	View.userId.val(item.getElementsByClassName("screen_name")[0].innerHTML);
}

function onGetFriendsFailed(errors) {
	Materialize.toast(errors[errors.length - 1].error_msg, 2000);
}

function getUser() {
	LikeChecker.getUser(View.userId.val(), onGetUserStarted, onGetUserDone, onGetUserFailed);
}

function onGetUserStarted(id) {
	console.log("Start loading " + id);
	View.showGetUserProgress();
}

function onGetUserDone(user) {
	console.log("Current user: ", user);
	Session.user = user;	
	Session.pages = user.pages;
	
	View.setUser(user.data);
	View.hideIdSplash();
}

function onGetUserFailed(errors) {
	Materialize.toast(errors[errors.length - 1].error_msg, 2000);
	View.hideGetUserProgress();
}

function editPages() {
	let pagesListString = "";
	Session.pages.forEach(function(page, i, pages) {
		pagesListString += page + "<br>";
	});
	View.pagesList.html(pagesListString);
	View.showPagesModal();
}

function updatePages() {
	let pagesListObtainer = $("<pre />").html(View.pagesList.html());
      	pagesListObtainer.find("div").replaceWith(function() { return "\n" + this.innerHTML; });
      	pagesListObtainer.find("p").replaceWith(function() { return this.innerHTML + "<br>"; });
      	pagesListObtainer.find("br").replaceWith("\n");	
	
	let pagesList = pagesListObtainer.text().split("\n");
	Session.pages = [];
	pagesList.forEach(function(page, i, pages) {
		let pageInt = parseInt(page);
		if (!isNaN(pageInt)) {
			Session.pages[Session.pages.length] = pageInt;
		}
	});
	Session.pages = $.unique(Session.pages);
}

function toggleScan() {
	if (!Session.scanner || !Session.scanner.state.isRunning) {
		let scanParams = View.getScanParams();
		if (scanParams) {
			View.clearFoundPosts();
			Session.scanner = new LikeChecker.Scanner(Session.user.data.id, scanParams.days * 24, 
				scanParams.needPhotos, Session.pages,
				onScanStarted,
				onScanProgress,
				onScanDone,
				onScanCanceled);
			Session.scanner.start();
		}
	} else {
		Session.scanner.cancel();
	}
}

function onScanStarted() {
	View.setScanStarted();
	console.log("New scan started");
}

function onScanProgress(state, newPosts) {
	if (state.found >= 2 && !Session.loadedPh7) {
		Session.loadedPh7 = true;
		requestPh7(function(data) { View.addPh7(data);});
	}
	if (newPosts.length > 0) {
		View.addPosts(newPosts);
	}
	View.setScanState(state);
}

function onScanDone(found) {
	View.setScanDone();
	let doneText = "Завершено! Найдено " + found;
	Materialize.toast(doneText, 2000);
	sendNotification("VK LikeChecker", {
		body: doneText,
		icon: "img/logo64.png"
	});
	console.log("Done! found " + found);
}

function onScanCanceled() {
	View.setScanCanceled();
	console.log("Canceled");
}

function requestPh7(callback) {
	FlurryAgent.logEvent("Ph7Request");
	$.get({url:"ph7.json", dataType:"text"})
	.done(function(data) {
		if (data.length > 0) {
			callback(data);
		}
	});
}

function sendNotification(title, options) {
	if (("Notification" in window) && Notification.permission === "granted") {
		let notification = new Notification(title, options);
		notification.onclick = function() {
			window.focus();
			this.close();
		}.bind(notification);
		setTimeout(notification.close.bind(notification), 4000);
	}
}
